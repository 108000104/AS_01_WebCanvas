# Software Studio 2021 Spring
## Assignment 01 Web Canvas
108000104陳若瑛
### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | N         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y        |


1.正方形，三角形，圓形可固定一點連續畫不同大小的形狀。
2.text input可以重複輸入用點的就ok，不用一直打。
3.橡皮擦跟正方形三角形圓形都可延續畫筆的顏色和大小，不是固定黑色。 




---
### How to use 
![](https://i.imgur.com/bQ2y1PR.png)

在一開始進入畫面後就可直接開始畫，初始設定是黑色筆刷，上方9個方格是9種顏色可供選擇(筆刷，正方形，三角形，長方型共用)，下方可橫向移動的是粗細(橡皮擦，筆刷，正方形，三角形，長方型共用)，左方eraser按下後為橡皮擦，reset按下後畫面會清空(當滑鼠移動到reset鍵我放了一個笑臉)，save image按下會存檔。

 ![](https://i.imgur.com/fTP8Pr5.png)

第二行都是控制text的功能，右邊的inputhere可以直接點他，然後輸入你想放入的字串，
再按下puttext，可隨意點畫面，將你輸入在inputhere裡的內容放置於你想放的地方，當你不想再放時，就點notput(記得要按，不然如果直接點筆刷會同時放入字串和筆刷)，然後可以再點其他功能繼續動作，若不點新的功能，則會回到上一個功能。而右邊6格是字的大小，旁邊的下拉選單是字體選擇，字的顏色為固定黑色，大小是獨立的，和剛剛的筆刷不共用。

![](https://i.imgur.com/IkD9QRQ.png)

左下角為redo undo功能跟特定形狀功能，按下undo會回到上一步，redo是你又後悔的時候可以再按回來，而右邊為正方形，圓形，三角形，這個功能我做了些比較特別的設定，若是你想要單獨畫很多個獨立的方形，那你畫完一個就要再點一次rect，而若是你想又有固定點，可延伸很多方形，則只需點一次rect，然後就可於畫面畫很多個。如下圖所示，右邊是有固定點的方形和圓形，左邊就是單獨個別畫的圖形。
 
而游標變化是當你點顏色後，會維持筆刷樣式，點橡皮擦後會維持橡皮擦，點inputhere會維持輸入的符號和點三個特殊形狀會有個小正方形。

以上是我所做的功能。
### Function description
第一個功能為固定點，畫不同大小的特殊形狀。
以方形為例，code部分我用ctx.rect(fix_x, fix_y, mousePos.x-fix_x, mousePos.y-fix_y);
在每次畫新的方形時，更改的為mousePos.x和y，而fix_x,y，我並沒有做更改，所以可以記錄第一下你點的位置，之後跟著新的座標繪製圖形。

第二功能為inputtext可不斷重複放置，這跟上面有點像，只是保留的是inputext，然後沒有更改功能，不斷更新新的滑鼠位置就可重複放置字串，想結束時就按notput，主要用來判斷是否要繼續。

第三功能為筆刷和橡皮擦和特殊圖形共用粗細與顏色，只是單純的於每次設定時呼叫相同變數。

### Gitlab page link
https://108000104.gitlab.io/AS_01_WebCanvas
your web page URL, which should be "https://[studentID].gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.

