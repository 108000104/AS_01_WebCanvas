var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');
var textcheck=0;
var t=0;

var fix_x;
var fix_y;

var undo_buffer = [];
var redo_buffer = [];
var r=0;
var c=0;
var a=0;
var undo, redo;

var wsize=10;
function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}
function mouseMove(evt) {
    var mousePos = getMousePos(canvas, evt);
    ctx.lineTo(mousePos.x, mousePos.y);
    ctx.stroke();
    
       
    if(r==2){
        var tmp;
        ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
        undo_buffer.push(tmp);
        ctx.beginPath();
        ctx.rect(fix_x, fix_y, mousePos.x-fix_x, mousePos.y-fix_y);
        ctx.stroke();
    
       }
  else if(c==2){
    var tmp;
			ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
			undo_buffer.push(tmp);
			ctx.beginPath();
			ctx.arc(fix_x, fix_y, Math.sqrt(Math.pow(mousePos.x-fix_x, 2)+Math.pow(mousePos.y-fix_y, 2)), 0, 2*Math.PI);
			ctx.stroke();
            
    }
  else if(a==2){
    var tmp;
			ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
			undo_buffer.push(tmp);
			ctx.beginPath();
			ctx.moveTo(fix_x, fix_y);
			ctx.lineTo(mousePos.x, mousePos.y);
			ctx.lineTo(2*fix_x-mousePos.x, mousePos.y);
			ctx.closePath();
			ctx.stroke();
            
        }
    else{
            var mousePos = getMousePos(canvas, evt);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.stroke();
        }
}
canvas.addEventListener('mousedown', function(evt) {

  var mousePos = getMousePos(canvas, evt);
  ctx.beginPath();
  ctx.moveTo(mousePos.x, mousePos.y);
  evt.preventDefault();
  undo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
		redo_buffer = [];

      

  canvas.addEventListener('mousemove', mouseMove, false);
  if(t==1){
     el = document.getElementById("font");
     ctx.font = wsize+"px "+el.options[el.selectedIndex].value;
     ctx.fillText(document.getElementById("input").value, mousePos.x, mousePos.y);
   }
   if(r==1){
    fix_x = mousePos.x;
    fix_y = mousePos.y;
    r=2;
   }
   if(c==1){
    fix_x = mousePos.x;
    fix_y = mousePos.y;
    c=2;
   }
   if(a==1){
    fix_x = mousePos.x;
    fix_y = mousePos.y;
    a=2;
   }
  
});

canvas.addEventListener('mouseup', function() {
  canvas.removeEventListener('mousemove', mouseMove, false);
  
}, false);

document.getElementById('reset').addEventListener('click', function() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}, false);

document.getElementById('eraser').addEventListener('click', function() {
   // console.log(currentMethod);
    ctx.globalCompositeOperation="destination-out";
    ctx.arc(lastX,lastY,8,0,Math.PI*2,false);
    
  }, false);
  document.getElementById('save').addEventListener('click', function() {
    var _url = canvas.toDataURL();
  //利用toDataURL() 把canvas轉成data:image
    this.href = _url;
  }, false);


var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white', 'ebebeb'];
var size = [3, 5, 10, 15, 20 , 30];
var sizeNames = ['three', 'five', 'ten', 'fifteen', 'twenty','threty'];

function listener(i) {
  document.getElementById(colors[i]).addEventListener('click', function() {
    ctx.globalCompositeOperation="source-over";
    ctx.strokeStyle = colors[i];
    
  }, false);
}
function fontSizes2() {
    ctx.lineWidth = document.getElementById("brush_size").value;
  }

function fontSizes(i) {
  document.getElementById(sizeNames[i]).addEventListener('click', function() {
    wsize = size[i];
  }, false);
}
for(var i = 0; i < size.length; i++) {
    fontSizes(i);
  }
  
for(var i = 0; i < colors.length; i++) {
  listener(i);
}


//

var lastX;
var lastY;
var strokeColor="red";
var strokeWidth=5;
var mouseX;
var mouseY;
var canvasOffset=$("#canvas").offset();
var offsetX=canvasOffset.left;
var offsetY=canvasOffset.top;
var isMouseDown=false;


function handleMouseDown(e){
  mouseX=parseInt(e.clientX-offsetX);
  mouseY=parseInt(e.clientY-offsetY);

  // Put your mousedown stuff here
  lastX=mouseX;
  lastY=mouseY;
  isMouseDown=true;
}

function handleMouseUp(e){
  mouseX=parseInt(e.clientX-offsetX);
  mouseY=parseInt(e.clientY-offsetY);

  // Put your mouseup stuff here
  isMouseDown=false;
}

function handleMouseOut(e){
  mouseX=parseInt(e.clientX-offsetX);
  mouseY=parseInt(e.clientY-offsetY);

  // Put your mouseOut stuff here
  isMouseDown=false;
}

function handleMouseMove(e){
  mouseX=parseInt(e.clientX-offsetX);
  mouseY=parseInt(e.clientY-offsetY);

  // Put your mousemove stuff here
  if(isMouseDown){
    ctx.beginPath();
    if(mode=="pen"){
      ctx.globalCompositeOperation="source-over";
      ctx.moveTo(lastX,lastY);
      ctx.lineTo(mouseX,mouseY);
      ctx.stroke();     
    }else{
      ctx.globalCompositeOperation="destination-out";
      ctx.arc(lastX,lastY,8,0,Math.PI*2,false);
      ctx.fill();
    }
    lastX=mouseX;
    lastY=mouseY;
  }
}

$("#canvas").mousedown(function(e){handleMouseDown(e);});
$("#canvas").mousemove(function(e){handleMouseMove(e);});
$("#canvas").mouseup(function(e){handleMouseUp(e);});
$("#canvas").mouseout(function(e){handleMouseOut(e);});

var mode="pen";
$("#pen").click(function(){ mode="pen"; });
$("#eraser").click(function(){ mode="eraser"; });

$("#bGenImage").click(function () {
    $("#dOutput").html(
    $("<img />", { src: $canvas[0].toDataURL(),
        "class": "output"
    }));
});
$('#save').on('click', function(){
    var _url = canvas.toDataURL();
    //利用toDataURL() 把canvas轉成data:image
    this.href = _url;
    //再把href載入上面的Data:image
  });

  beeGame.mouseMove = function(mouseMovement) {
    // when the mouse moves, set xPosition and yPosition to the x and y of the cursor,
    // then assign those x and y to the bee in the css file (position absolute, use top and left)
    const bee = document.getElementById('bee');
    let xPosition;
    let yPosition;
    if (mouseMovement) {
        xPosition = mouseMovement.pageX;
        yPosition = mouseMovement.pageY;
        bee.style.top = yPosition + 1 + 'px';
        // added 1 pixel to get the bee off of the cursor itself so you're clicking on what you want to click on not the bee image
        bee.style.left = xPosition + 'px';
    };
};

function brush(){
	document.getElementsByTagName("body")[0].style.cursor = "url(brush.png), auto";
    r=0;
  c=0;
  a=0;
}
function eraser(){
	document.getElementsByTagName("body")[0].style.cursor = "url(eraser.png), auto";
    r=0;
  c=0;
  a=0;
}
function text(){
    document.getElementsByTagName("body")[0].style.cursor = "url(img/rect.png), text";
    r=0;
  c=0;
  a=0;
}
function text2(){
    document.getElementsByTagName("body")[0].style.cursor = "url(img/rect.png), text";
    t=1;
    r=0;
  c=0;
  a=0;
;}

function text3(){
    document.getElementsByTagName("body")[0].style.cursor = "url(default), auto";
    t=0;
    r=0;
  c=0;
  a=0;
;}

function r1(){
    document.getElementsByTagName("body")[0].style.cursor = "url(rect.png), auto";
    r=1;
  c=0;
  a=0;
    ;}
function c1(){
    r=0;
  c=1;
  a=0;
    document.getElementsByTagName("body")[0].style.cursor = "url(rect.png), auto";
    
    ;}
function a1(){
    r=0;
  c=0;
  a=1;
    document.getElementsByTagName("body")[0].style.cursor = "url(rect.png), auto";
   
;}

function undo(){
    if(undo_buffer.length>0){
        redo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        ctx.putImageData(undo_buffer.pop(), 0, 0);
        r=0;
  c=0;
  a=0;
    }
}

function redo(){
    if(redo_buffer.length>0){
        undo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        ctx.putImageData(redo_buffer.pop(), 0, 0);
        r=0;
  c=0;
  a=0;
    }
}