var mousedown = false;
var currentMethod = "none";
var brush_shape = "round";
var fix_x;
var fix_y;

var undo_buffer = [];
var redo_buffer = [];

var reset, undo, redo, download;

window.addEventListener("load", ()=>{
	const canvas = document.getElementById("canvas");
	const ctx = canvas.getContext("2d");
	canvas.width = 1280;
	canvas.height = 720;

	canvas.addEventListener("mousedown", (e)=>{
		mousedown = true;
		undo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
		redo_buffer = [];
		if(currentMethod == "brush"){
			ctx.strokeStyle = document.getElementById("color").value;
			ctx.lineWidth = document.getElementById("brush_size").value;
			ctx.lineCap = brush_shape;
			ctx.beginPath();
			ctx.moveTo(e.offsetX, e.offsetY);
			ctx.lineTo(e.offsetX, e.offsetY);
			ctx.stroke();
		} else if(currentMethod == "eraser"){
			ctx.strokeStyle = "white";
			ctx.lineWidth = document.getElementById("brush_size").value;
			ctx.lineCap = brush_shape;
			ctx.beginPath();
			ctx.moveTo(e.offsetX, e.offsetY);
			ctx.lineTo(e.offsetX, e.offsetY);
			ctx.stroke();
		} else if(currentMethod == "rect"){
			ctx.strokeStyle = document.getElementById("color").value;
			ctx.lineWidth = document.getElementById("brush_size").value;
			ctx.lineCap = brush_shape;
			fix_x = e.offsetX;
			fix_y = e.offsetY;
		} else if(currentMethod == "rectblock"){
			ctx.fillStyle = document.getElementById("color").value;
			fix_x = e.offsetX;
			fix_y = e.offsetY;
		} else if(currentMethod == "circle"){
			ctx.strokeStyle = document.getElementById("color").value;
			ctx.lineWidth = document.getElementById("brush_size").value;
			ctx.lineCap = brush_shape;
			fix_x = e.offsetX;
			fix_y = e.offsetY;
		} else if(currentMethod == "circleblock"){
			ctx.fillStyle = document.getElementById("color").value;
			fix_x = e.offsetX;
			fix_y = e.offsetY;
		} else if(currentMethod == "triangle"){
			ctx.strokeStyle = document.getElementById("color").value;
			ctx.lineWidth = document.getElementById("brush_size").value;
			ctx.lineCap = brush_shape;
			fix_x = e.offsetX;
			fix_y = e.offsetY;
		} else if(currentMethod == "triangleblock"){
			ctx.fillStyle = document.getElementById("color").value;
			fix_x = e.offsetX;
			fix_y = e.offsetY;
		} else if(currentMethod == "line"){
			ctx.strokeStyle = document.getElementById("color").value;
			ctx.lineWidth = document.getElementById("brush_size").value;
			ctx.lineCap = brush_shape;
			fix_x = e.offsetX;
			fix_y = e.offsetY;
		} else if(currentMethod == "text"){
			ctx.fillStyle = document.getElementById("color").value;
			el = document.getElementById("font");
			ctx.font = document.getElementById("font_size").value+"px "+el.options[el.selectedIndex].value;
			ctx.fillText(document.getElementById("text").value, e.offsetX, e.offsetY);
		}
	});

	canvas.addEventListener("mousemove", (e)=>{
		if(!mousedown)
			return ;

		if(currentMethod == "brush"){
			ctx.lineTo(e.offsetX, e.offsetY);
			ctx.stroke();
			ctx.beginPath();
			ctx.moveTo(e.offsetX, e.offsetY);
		} else if(currentMethod == "eraser"){
			ctx.lineTo(e.offsetX, e.offsetY);
			ctx.stroke();
			ctx.beginPath();
			ctx.moveTo(e.offsetX, e.offsetY);
		} else if(currentMethod == "rect"){
			var tmp;
			ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
			undo_buffer.push(tmp);
			ctx.beginPath();
			ctx.rect(fix_x, fix_y, e.offsetX-fix_x, e.offsetY-fix_y);
			ctx.stroke();
		} else if(currentMethod == "rectblock"){
			ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
			undo_buffer.push(tmp);
			ctx.fillRect(fix_x, fix_y, e.offsetX-fix_x, e.offsetY-fix_y);
			ctx.stroke();
		} else if(currentMethod == "circle"){
			var tmp;
			ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
			undo_buffer.push(tmp);
			ctx.beginPath();
			ctx.arc(fix_x, fix_y, Math.sqrt(Math.pow(e.offsetX-fix_x, 2)+Math.pow(e.offsetY-fix_y, 2)), 0, 2*Math.PI);
			ctx.stroke();
		} else if(currentMethod == "circleblock"){
			var tmp;
			ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
			undo_buffer.push(tmp);
			ctx.beginPath();
			ctx.arc(fix_x, fix_y, Math.sqrt(Math.pow(e.offsetX-fix_x, 2)+Math.pow(e.offsetY-fix_y, 2)), 0, 2*Math.PI);
			ctx.fill();
		} else if(currentMethod == "triangle"){
			var tmp;
			ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
			undo_buffer.push(tmp);
			ctx.beginPath();
			ctx.moveTo(fix_x, fix_y);
			ctx.lineTo(e.offsetX, e.offsetY);
			ctx.lineTo(2*fix_x-e.offsetX, e.offsetY);
			ctx.closePath();
			ctx.stroke();
		} else if(currentMethod == "triangleblock"){
			var tmp;
			ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
			undo_buffer.push(tmp);
			ctx.beginPath();
			ctx.moveTo(fix_x, fix_y);
			ctx.lineTo(e.offsetX, e.offsetY);
			ctx.lineTo(2*fix_x-e.offsetX, e.offsetY);
			ctx.closePath();
			ctx.fill();
		} else if(currentMethod == "line"){
			var tmp;
			ctx.putImageData(tmp=undo_buffer.pop(), 0, 0);
			undo_buffer.push(tmp);
			ctx.beginPath();
			ctx.moveTo(fix_x, fix_y);
			ctx.lineTo(e.offsetX, e.offsetY);
			ctx.stroke();
		}
	});

	window.addEventListener("mouseup", ()=>{
		mousedown = false;
		ctx.beginPath();
	});

	reset = function(){
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	}

	undo = function(){
		if(undo_buffer.length>0){
			redo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
			ctx.putImageData(undo_buffer.pop(), 0, 0);
		}
	}

	redo = function(){
		if(redo_buffer.length>0){
			undo_buffer.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
			ctx.putImageData(redo_buffer.pop(), 0, 0);
		}
	}

	document.getElementById("download").addEventListener("click", ()=>{
		document.getElementById("download").setAttribute("href", canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"))
	});
});

function brush(){
	currentMethod="brush";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/brush.png), auto";
}

function eraser(){
	currentMethod="eraser";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/eraser.png), auto";
}

function rect(){
	currentMethod="rect";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/rect.png), auto";
}

function rect_block(){
	currentMethod="rectblock";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/rect_block.png), auto";
}

function circle(){
	currentMethod="circle";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/circle.png), auto";
}

function circle_block(){
	currentMethod="circleblock";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/circle_block.png), auto";
}

function triangle(){
	currentMethod="triangle";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/triangle.png), auto";
}

function triangle_block(){
	currentMethod="triangleblock";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/triangle_block.png), auto";
}

function line(){
	currentMethod="line";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/line.png), auto";
}

function text(){
	currentMethod="text";
	console.log(currentMethod);
	document.getElementsByTagName("body")[0].style.cursor = "url(img/text.png), text";
}
